# Avaliação SoluCX - Olívio Rodrigues

> Desenvolvimento Front-End utilizando VueJS2.

## Como executar ?

``` bash
# instalar as dependencias.
npm install

# startar em localhost:8080 - padrão.
npm run dev

# build para produção.
npm run build

```
